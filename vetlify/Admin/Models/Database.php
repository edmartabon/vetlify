<?php

namespace Vetlify\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class Database extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @param array
     */
    protected $fillable = ['name', 'host', 'port', 'description'];
}
