<?php

namespace Vetlify\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class AccountType extends Model
{
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @param array
     */
    protected $fillable = ['name', 'limit_branch', 'limit_product', 'limit_transaction'];
}
