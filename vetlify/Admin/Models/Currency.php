<?php

namespace Vetlify\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'currencies';

    /**
     * The attributes that are mass assignable.
     *
     * @param array
     */
    protected $fillable = ['name', 'symbol', 'symbol_native', 'decimal_digits', 'rounding', 'code', 'name_plural'];
}
