<?php

namespace Vetlify\Admin\Models;

use Illuminate\Database\Eloquent\Model;

class AccountStatus extends Model
{
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The table associated with the model.
     *
     * @param string
     */
    protected $table = 'account_status';

    /**
     * The attributes that are mass assignable.
     *
     * @param array
     */
    protected $fillable = ['name'];
}
