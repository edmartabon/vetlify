<?php

namespace Vetlify\Database\Providers;

use Route;
use Config;
use Illuminate\Support\ServiceProvider;

class DatabaseServiceProvider extends ServiceProvider
{

    /**
     * Get the main base path of packages;.
     */
    private $rootPath = __DIR__ . '/../';

    /**
     * New instance of vetlify database
     */
    private $vetlifyDatabaseInstance;


    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->init();
        $this->registerConsole();
        $this->registerVetlifyDatabase();
        $this->registerVetlifyDatabaseContract();
        $this->addAlias();
    }

    /**
     * Initiate database configuration
     *
     * @return void
     */
    private function init()
    {
        $this->vetlifyDatabaseInstance = new \Vetlify\Database\Services\VetlifyDatabase;
    }

    /**
     * Register database console
     *
     * @return void
     */
    private function registerConsole()
    {
        $consoles = [];
        foreach (glob($this->rootPath . 'Consoles/*') as $filename) {
            array_push($consoles, '\Vetlify\\'.basename(realpath($this->rootPath)).'\Consoles\\'.pathinfo($filename)['filename']);
        }
        $this->commands($consoles);
    }

    /**
     * Register vetlify database service
     *
     * @return void
     */
    private function registerVetlifyDatabase()
    {
        $vetlifyDatabaseIntance = $this->vetlifyDatabaseInstance;

        $this->app->bind('VetlifyDatabase', function () use ($vetlifyDatabaseIntance) {
            return $vetlifyDatabaseIntance;
        });
    }

    /**
     * Register vetlify database service
     *
     * @return void
     */
    private function registerVetlifyDatabaseContract()
    {
        $vetlifyDatabaseIntance = $this->vetlifyDatabaseInstance;

        $this->app->bind('Vetlify\Database\Contracts\VetlifyDatabaseContract', function () use ($vetlifyDatabaseIntance) {
            return $vetlifyDatabaseIntance;
        });
    }
    
    /**
     * Load the alias of vetlify database
     *
     * @return void
     */
    private function addAlias()
    {
        $loader = \Illuminate\Foundation\AliasLoader::getInstance();

        $loader->alias('VetlifyDatabase', 'Vetlify\Database\Supports\Facades\VetlifyDatabase');
    }
}
