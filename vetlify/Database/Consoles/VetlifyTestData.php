<?php

namespace Vetlify\Database\Consoles;

use Faker;
use Sentinel as UserSentinel;
use Illuminate\Console\Command;
use Vetlify\Client\Models\Branch;
use Vetlify\Client\Models\Client;
use Vetlify\Client\Models\Patient;
use Vetlify\Client\Models\PatientGender;
use Vetlify\Database\Contracts\VetlifyDatabaseContract;

class VetlifyTestData extends Command
{
    
    /**
     * Get the main base path of packages;.
     */
    private $basePath = __DIR__ . '/../';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vetlify:test-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param VetlifyDatabaseContract $vetlifyDatabase
     * @return void
     */
    public function handle(VetlifyDatabaseContract $vetlifyDatabase)
    {
        $this->registerNewUser($vetlifyDatabase);
        $this->createFakerClientAndPatient($vetlifyDatabase);
    }

    /**
     * Register new user for new test account
     *
     * @param Vetlify\Database\Contracts\VetlifyDatabaseContract
     * @return void
     */
    private function registerNewUser(VetlifyDatabaseContract $vetlifyDatabase)
    {
        $vetlifyDatabase->changeDefaultConfigConnection(['database' => 'vet']);
        $branch = Branch::create([
            'name' => 'Bizwex',
            'address_one' => 'Balanga, Bataan'
        ]);

        $user = UserSentinel::registerAndActivate([
            'email' 	 => 'bizwex@gmail.com',
            'password'   => '123456789',
            'first_name' => 'bizwex',
            'last_name'  => 'vetlify',
            'branch_id'  => $branch->id,
            'user_status_id' => 1
        ]);

        $adminRole = UserSentinel::findRoleByName('Admin');
        $adminRole->users()->attach($user);
    }

    /**
     * Create fake client and patient
     *
     * @param Vetlify\Database\Contracts\VetlifyDatabaseContract $vetlifyDatabaseContract
     * @return void
     */
    private function createFakerClientAndPatient(VetlifyDatabaseContract $vetlifyDatabase)
    {
        $vetlifyDatabase->changeDefaultConfigConnection(['database' => 'vet']);
        $faker = Faker\Factory::create();
        $totalData = 1000;
        $bar = $this->output->createProgressBar($totalData);

        for ($i=0;$i<$totalData;$i++) {
            $patient = Patient::create([
                'patient_record_id' => $faker->numberBetween(100000, 999999),
                'first_name' => $faker->name,
                'last_name' => $faker->lastName,
                'rfid' => $this->randomKey(20),
                'branch_id' => 1,
                'patient_blood_id' => 1,
                'birth_date' => date('Y-m-d H:i:s'),
                'patient_gender_id' => 1,
            ]);


            $client = Client::create([
                'first_name' => $faker->name,
                'last_name' => $faker->lastName,
                'address_one' => $faker->streetAddress,
                'phone_no' => $faker->phoneNumber,
                'telephone_no' => $faker->phoneNumber,
                'rfid' => $this->randomKey(20),
                'patient_id' => $patient->id,
                'branch_id' => 1,
                'patient_gender_id' => 1,
            ]);

            $bar->advance();
        }

        $bar->finish();
    }

    /**
     * Generate ramdom key
     */
    private function randomKey($length)
    {
        $pool = array_merge(range(0, 9), range('a', 'z'), range('A', 'Z'));
        $key = '';
        for ($i=0; $i < $length; $i++) {
            $key .= $pool[mt_rand(0, count($pool) - 1)];
        }
        return $key;
    }
}
