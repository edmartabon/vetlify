<?php

namespace Vetlify\Database\Consoles;

use Illuminate\Console\Command;
use Vetlify\Admin\Models\Account;
use Vetlify\Admin\Models\AccountType;
use Vetlify\Admin\Models\AccountStatus;
use Vetlify\Database\Contracts\VetlifyDatabaseContract;

class VetlifyMigration extends Command
{
    
    /**
     * Get the main base path of packages;.
     */
    private $basePath = __DIR__ . '/../';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vetlify:migrate {--type= : Set the type for custom migration }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Manage the databases';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param VetlifyDatabaseContract $vetlifyDatabase
     * @return void
     */
    public function handle(VetlifyDatabaseContract $vetlifyDatabase)
    {
        $acceptType = ['admin', 'client'];

        if (!is_null($this->option('type')) && in_array($this->option('type'), $acceptType)) {
            $methodType = $this->option('type').'Migration';
            $this->$methodType($vetlifyDatabase);
        }
    }

    /**
     * Execute the client migration.
     *
     * @param VetlifyDatabaseContract $vetlifyDatabase
     * @return void
     */
    private function clientMigration(VetlifyDatabaseContract $vetlifyDatabase)
    {
        $vetlifyDatabase->changeDefaultConfigConnection(['database' => $vetlifyDatabase->getAdminDatabase()]);
        $accounts = Account::leftJoin('databases', 'accounts.database_id', '=', 'databases.id')
            ->select('accounts.server', 'databases.host', 'databases.port')
            ->get()->toArray();

        foreach ($accounts as $account) {
            $account['database'] = $account['server'];
            unset($account['server']);
            $vetlifyDatabase->createClientDatabase($account);
            $vetlifyDatabase->migrateClientMigration($account);
        }
    }

    /**
     * Execute the admin migration.
     *
     * @param VetlifyDatabaseContract $vetlifyDatabase
     * @return void
     */
    private function adminMigration(VetlifyDatabaseContract $vetlifyDatabase)
    {
        $vetlifyDatabase->migrateAdminMigration();
    }
}
