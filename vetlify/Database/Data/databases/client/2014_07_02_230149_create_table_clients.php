<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableClients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_genders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        Schema::create('patients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('patient_record_id');
            $table->string('first_name');
            $table->string('last_name')->nullable();
            $table->string('rfid')->nullable();
            $table->string('avatar')->nullable();
            $table->integer('branch_id')->unsigned();
            $table->integer('patient_blood_id')->unsigned();
            $table->integer('patient_gender_id')->unsigned();
            $table->timestamp('birth_date');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('branch_id')->references('id')->on('branches');
            $table->foreign('patient_blood_id')->references('id')->on('patient_bloods');
            $table->foreign('patient_gender_id')->references('id')->on('patient_genders');
        });

        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('phone_no')->nullable();
            $table->string('telephone_no')->nullable();
            $table->string('address_one')->nullable();
            $table->string('address_two')->nullable();
            $table->string('rfid')->nullable();
            $table->string('avatar')->nullable();
            $table->integer('patient_id')->unsigned();
            $table->integer('branch_id')->unsigned();
            $table->integer('patient_gender_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('branch_id')->references('id')->on('branches');
            $table->foreign('patient_id')->references('id')->on('patients');
            $table->foreign('patient_gender_id')->references('id')->on('patient_genders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('clients');
        Schema::drop('patients');
        Schema::drop('patient_genders');
    }
}
