<?php

use Sentinel as UserSentinel;
use Vetlify\Client\Models\UserStatus;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class MigrationSeederInitial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->process([
            '\Vetlify\Client\Models\UserStatus' => 'addUserStatus',
            '\Vetlify\Client\Models\PatientBlood' => 'addPatientBlood',
            '\Vetlify\Client\Models\PatientGender' => 'addPatientGender',
        ]);
        $this->sentinelSeeder();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }

    /**
     * Save all the seeder
     *
     * @param string $methods
     * @return void
     */
    private function process($methods)
    {
        foreach ($methods as $model => $method) {
            foreach ($this->$method() as $params) {
                $model::create($params);
            }
        }
    }

    /**
     * Add new user status
     *
     * @return array
     */
    private function addUserStatus()
    {
        return [
            ['name' => 'Enable'],
            ['name' => 'Disable']
        ];
    }

    /**
     * Add new patient's blood
     *
     * @return array
     */
    private function addPatientBlood()
    {
        return [
            ['name' => 'O+'],
            ['name' => 'O-'],
            ['name' => 'A+'],
            ['name' => 'A-'],
            ['name' => 'B+'],
            ['name' => 'B-'],
            ['name' => 'AB+'],
            ['name' => 'AB-'],
            ['name' => 'A1+'],
            ['name' => 'A1-'],
            ['name' => 'A1B+'],
            ['name' => 'A1B-'],
            ['name' => 'A2+'],
            ['name' => 'A2-'],
            ['name' => 'A2B+'],
            ['name' => 'A2B-'],
            ['name' => 'B1+'],
            ['name' => 'Other']
        ];
    }

    /**
     * Add new patient's genders
     *
     * @return array
     */
    private function addPatientGender()
    {
        return [
            ['name' => 'Male'],
            ['name' => 'Female']
        ];
    }

    /**
     * Seed the sentinel permission
     *
     * @return void
     */
    private function sentinelSeeder()
    {
        UserSentinel::getRoleRepository()->createModel()->create([
            'name' => 'Admin',
            'slug' => 'admin',
            'permissions' => [
                'admin.read' => true,
                'admin.create' => true,
                'admin.update' => true,
                'admin.delete' => true
            ]
        ]);

        UserSentinel::getRoleRepository()->createModel()->create([
            'name' => 'Veterinarian',
            'slug' => 'vet',
            'permissions' => [
                'vet.read' => true,
                'vet.create' => true,
                'vet.update' => true,
                'vet.delete' => true
            ]
        ]);

        UserSentinel::getRoleRepository()->createModel()->create([
            'name' => 'Secretary',
            'slug' => 'sec',
            'permissions' => [
                'sec.read' => true,
                'sec.create' => true,
                'sec.update' => true,
                'sec.delete' => true
            ]
        ]);

        UserSentinel::getRoleRepository()->createModel()->create([
            'name' => 'Cashier',
            'slug' => 'cashier',
            'permissions' => [
                'cashier.read' => true,
                'cashier.create' => true,
                'cashier.update' => true,
                'cashier.delete' => true
            ]
        ]);
    }
}
