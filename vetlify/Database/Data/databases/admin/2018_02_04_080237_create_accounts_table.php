<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('server')->unique();
            $table->integer('database_id')->unsigned();
            $table->integer('account_type_id')->unsigned();
            $table->integer('account_status_id')->unsigned();
            $table->timestamps();
            $table->timestamp('expired_at')->nullable();

            $table->foreign('database_id')->references('id')->on('databases');
            $table->foreign('account_type_id')->references('id')->on('account_types');
            $table->foreign('account_status_id')->references('id')->on('account_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
