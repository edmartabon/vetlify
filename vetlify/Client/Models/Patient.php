<?php

namespace Vetlify\Client\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Patient extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @param array
     */
    protected $fillable = ['patient_record_id', 'first_name', 'last_name', 'rfid', 'avatar', 'branch_id', 'patient_gender_id', 'patient_blood_id', 'birth_date'];
}
