<?php

namespace Vetlify\Client\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @param array
     */
    protected $fillable = ['first_name', 'last_name', 'phone_no', 'telephone_no', 'patient_id', 'patient_gender_id', 'address_one', 'address_two', 'rfid', 'avatar', 'branch_id'];
}
