<?php

namespace Vetlify\Client\Models;

use Illuminate\Database\Eloquent\Model;

class UserStatus extends Model
{

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The table associated with the model.
     *
     * @param string
     */
    protected $table = 'user_status';

    /**
     * The attributes that are mass assignable.
     *
     * @param array
     */
    protected $fillable = ['name'];
}
