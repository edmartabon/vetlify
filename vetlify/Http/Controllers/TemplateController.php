<?php

namespace Vetlify\Http\Controllers;

use Sentinel;
use Illuminate\Http\Request;
use Vetlify\Client\Models\Patient;
use App\Http\Controllers\Controller;
use Vetlify\Http\Requests\Clients\ClientIndexRequest;

class TemplateController extends Controller
{
    /**
     * Show the client page
     *
     * @return void
     */
    public function clientList(Request $request, ClientIndexRequest $clientIndexRequest)
    {
        return view('Vetlify::pages.admin.client.client');
    }

    /**
     * Show the client page
     *
     * @return void
     */
    public function clientProfile(Request $request, ClientIndexRequest $clientIndexRequest)
    {
        return view('Vetlify::pages.admin.client.profile');
    }
}
