<?php

namespace Vetlify\Http\Controllers;

use Sentinel;
use Illuminate\Http\Request;
use Vetlify\Client\Models\Patient;
use App\Http\Controllers\Controller;
use Vetlify\Http\Requests\Clients\ClientIndexRequest;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, ClientIndexRequest $clientIndexRequest)
    {
        $user = Sentinel::getUser();
        $patient = Patient::leftJoin('clients', 'clients.patient_id', '=', 'patients.id')
            ->where('patients.branch_id', $user->branch_id);

        if ($request->start && $request->end) {
            $patient = $patient->where([
                ['patients.created_at', '>=', date('Y-m-d 00:00:00', strtotime($request->start))],
                ['patients.created_at', '<=', date('Y-m-d 23:59:59', strtotime($request->end))]
            ]);
        }

        if ($request->gender) {
            $patient = $patient->where([
                'patients.patient_gender_id' => $request->gender
            ]);
        }

        if ($request->search) {
            $search = strtolower($request->search);
            $patient = $patient->where(function ($query) use ($search, $request) {
                foreach (explode(' ', $search) as $searchItem) {
                    $query->orWhereRaw('LOWER(patients.first_name) LIKE ?', "%$searchItem%");
                    $query->orWhereRaw('LOWER(patients.last_name) LIKE ?', "%$searchItem%");
                    $query->orWhereRaw('LOWER(clients.first_name) LIKE ?', "%$searchItem%");
                    $query->orWhereRaw('LOWER(clients.last_name) LIKE ?', "%$searchItem%");
                    $query->orWhereRaw('LOWER(patients.patient_record_id) LIKE ?', "%$searchItem%");
                }
                $query->orWhere('patients.rfid', '=', $request->search);
            });
        }

        $patient = $patient
            ->select([
                'patients.id',
                'patients.patient_record_id',
                'patients.first_name AS patient_first_name',
                'patients.last_name AS patient_last_name',
                'clients.first_name AS client_first_name',
                'clients.last_name AS client_last_name',
            ])
            ->orderBy('patients.id', 'desc')
            ->paginate(20);

        return $patient;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        return Patient::where('id', $request->route('id'))->delete();
    }
}
