@extends('Vetlify::layouts.admin')

@section('page_title', 'Clients')

@section('content')
<!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Clients
        <small>List of client and patients</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-paw"></i> Home</a></li>
        <li><a href="#">Client</a></li>
      </ol>
    </section>
  
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-9">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Client List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="loader"></div>
              <table class="table table-bordered table-view">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>ID</th>
                    <th>Patient Name</th>
                    <th>Owner</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                    <tr>
                      <td colspan="5">No client to show.</td>
                    </tr>
                </tbody>
              </table>

              <div class="pull-right">
                <nav aria-label="Page navigation">
                  <ul class="pagination" id="pagination"></ul>
                </nav>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <div class="col-md-3">
          <a href="mailbox.html" class="btn btn-primary btn-block margin-bottom">Create Patient</a>
          
          <div class="box box-solid">
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li><a href="#" class="filter-reset"><i class="fa fa-undo text-blue"></i> Clear Search</a></li>
              </ul>
            </div>
          </div>

          <div class="box box-solid">
            <div class="box-body no-padding">
              <div class="col-md-10 no-padding">
                <input type="text" class="form-control filter-search-value" placeholder="Search...">
              </div>
              <div class="col-md-2 no-padding">
                <button type="button" class="btn btn-info filter-search">
                  <span class="glyphicon glyphicon-search"></span>
                </button>
              </div>
            </div>
          </div>

          <div class="box box-solid">
            <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
              <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
              <span></span> <b class="caret"></b>
            </div>
            <div class="clearfix"></div>
          </div>

          <div class="box box-solid">          

            <div class="box-header with-border">
              <h3 class="box-title">Date</h3>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li><a href="#" class="filter-date" data-start="{{ date('Y-m-d') }}"  data-end="{{ date('Y-m-d') }}"><i class="fa fa-circle-o text-blue"></i> Today</a></li>
                <li><a href="#" class="filter-date" data-start="{{ date('Y-m-d', strtotime('-1 day')) }}"  data-end="{{ date('Y-m-d') }}"><i class="fa fa-circle-o text-yellow"></i> Yesterday</a></li>
                <li><a href="#" class="filter-date" data-start="{{ date('Y-m-d', strtotime('-6 day')) }}"  data-end="{{ date('Y-m-d') }}"><i class="fa fa-circle-o text-aqua"></i> Last 7 Days</a></li>
                <li><a href="#" class="filter-date" data-start="{{ date('Y-m-d', strtotime('-29 day')) }}"  data-end="{{ date('Y-m-d') }}"><i class="fa fa-circle-o text-light-blue"></i> Last 30 Days</a></li>
              </ul>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /. box -->
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Gender</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li><a href="#" class="filter-gender" data-id="1"><i class="fa fa-circle-o text-red"></i> Male</a></li>
                <li><a href="#" class="filter-gender" data-id="2"><i class="fa fa-circle-o text-yellow"></i> Female</a></li>
              </ul>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

    <div class="modal fade" id="modal-delete">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Delete Patient</h4>
          </div>
          <div class="modal-body">
            <p>Are you sure you want to delete this patient?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-danger btn-delete-confirm">Delete</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    
@endsection

@section('script')
<script type="text/javascript" src="/js/pagination.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {

  var filterItem = {};

  const filter = (params) => {
    $('.loader').removeClass('hide');
    if (typeof params === 'undefined') {
      filterItem['page'] = 1;
    }

    $.ajax({
      type: 'GET',
      url: '/app/api/clients',
      data: $.param(filterItem),
      success: function(data) {
        if (typeof params === 'undefined') {
           pager(data.total, data.per_page);
        }
        view(data);
        $('.loader').addClass('hide');
      }
    });
  }

  const view = (data) => {
    var num = 0;
    var viewTable = $('.table-view tbody');
    viewTable.html('');

    if (data.data.length > 0) {
      data.data.forEach(item => {
        viewTable.append(`
          <tr>
            <td>` + (data.per_page *(data.current_page-1)+(num)+ 1) + `.</td>
            <td>` + item.patient_record_id + `</td>
            <td><a href="/{{ Config::get('vetlify.app_route_prefix') }}/clients/profile/` + item.id + `">` + item.patient_first_name + ' ' + item.patient_last_name + `</a></td>
            <td>` + item.client_first_name + ' ' + item.client_last_name + `</td>
            <td>
              <button type="button" data-id="` + item.id + `" class="btn btn-primary btn-sm btn-view"><i class="fa fa-eye"></i> View</button>
              <button type="button" data-id="` + item.id + `" class="btn btn-danger btn-sm btn-delete" data-toggle="modal" data-target="#modal-delete"><i class="fa fa-remove"></i> Delete</button>
            </td>
          </tr>
        `);
        num++;
      });
    }
    
  }

  const datePicker = () => {
    var start = moment().subtract(29, 'days');
    var end = moment();

    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

    $('#reportrange').daterangepicker({
      startDate: start,
      endDate: end
    }, (start, end) => {
      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      filterItem['start'] = start.format('YYYY-MM-DD');
      filterItem['end'] = end.format('YYYY-MM-DD');
      filter();
    });
  }

  const pager = (item, itemOnPage) => {
    $('.pagination').pagination({
      items: item,
      itemsOnPage: itemOnPage,
      onPageClick: function (page) {
        filterItem['page'] = page;
        filter({ pager: true });
      }
    });
  }

  $(document).on('click', '.filter-reset', function() {
    filterItem = {};
    filter();
  });

  $(document).on('click', '.filter-search', function() {
    filterItem['search'] = $('.filter-search-value').val();
    filter();
  });

  $(document).on('click', '.filter-date', function() {
    filterItem['start'] = $(this).data('start');
    filterItem['end'] = $(this).data('end');
    filter();
  });

  $(document).on('click', '.filter-gender', function() {
    filterItem['gender'] = $(this).data('id');
    filter();
  });

  $(document).on('click', '.btn-delete', function() {
    var parentTr = $(this).parent().parent(),
      btnDelete = $('.btn-delete-confirm');

    btnDelete.data('id', $(this).data('id'));
    btnDelete.data('index', parentTr.index());
  });

  $(document).on('click', '.btn-delete-confirm', function() {
    var _this = $(this);
    _this.removeClass('btn-delete-confirm');

    $.ajax({
      type: 'DELETE',
      url: '/app/api/clients/' + _this.data('id'),
      data: 'id=' + _this.data('id'),
      success: function(data) {
        _this.addClass('btn-delete-confirm');
        $('.table-view tbody tr').eq(_this.data('index')).remove();

        $('#modal-delete').modal('hide');
      },
      error: function(data) {
        _this.addClass('btn-delete-confirm');
        $('#modal-delete').modal('hide');
      }
    });
  });

  $(document).on('click', '.btn-view', function() {
    window.location = '/{{Config::get("vetlify.app_route_prefix")}}/clients/profile/' + $(this).data('id');
  });

  $('.filter-search-value').keydown(function(e) {
    if (e.keyCode == 13) {
      $('.filter-search').trigger('click');
    }
  });

  datePicker();
  filter();
});
</script>
@endsection