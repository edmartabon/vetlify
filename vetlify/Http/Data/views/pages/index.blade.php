@extends('Vetlify::layouts.master')

@section('page-title', 'Vetlify - Veterinary Clinic Management System')

@section('contents')
	
	<!-- 
	======================================================= 
	    Introduction
	=======================================================
	-->
	<div id="intro" class="full-container intro-container">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-12 intro-contents">
					<header>
						<h1 id="introducing">Introducing <span>Vetlify <span class="beta">Beta</span></span></h1>
						<p class="p-intro">A web-based veterinary clinic management program designed to make managing your veterinary clinic easy, intuitive and fun.</p>
					</header>
				</div>
			</div>
		</div>
	</div>

	<!-- 
	======================================================= 
	    Main Image Banner
	=======================================================
	-->
	<div class="image-banner full-container">
		<img src="images/imac-macbook.png" alt="App Representation">
	</div>

	<!-- 
	======================================================= 
	    Signup & Contact Link
	=======================================================
	-->
	<div class="signup-container full-container">
		<div class="container-fluid">
			<div class="row">
				<div class="link-container center-block">
					<a id="contact-section" href="#contact-us" class="contact-link contact-btn">Contact Us</a>
					<a href="http://vetapp.dev/index.html#!/login/register" class="signup-link">Sign-up for free</a>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>

	<!-- 
	======================================================= 
	    Feature Highlights
	=======================================================
	-->
	<div id="three-col-container" class="full-container feature-highlights-container">
		<div class="container-fluid">
			<div class="row">
				<section class="col-sm-12 highlights-wrapper">
					<ul>
						<li class="feature-1">
							<div class="media-left">
								<i class="icon-settings-streamline"></i>
							</div>

							<div class="media-right">
								<h3>Interactive Physical Checkup</h3>
								<p>Using image mapping, patient records are associated with the exact areas affected; creating a unique user experience.</p>
							</div>
						</li>
						<li class="feature-2">
							<div class="media-left">
								<i class="icon-calendar"></i>
							</div>

							<div class="media-right">
								<h3>Real-time Appointments</h3>
								<p>Clients can make appointments online which is reflected real-time on the clinic app.</p>
							</div>
						</li>
						<li class="feature-3">
							<div class="media-left">
								<i class="icon-credit-card"></i>
							</div>

							<div class="media-right">
								<h3>Built-in Point-of-Sale System</h3>
								<p>Selling medicine and other supplies is easy using the POS system included in the app.</p>
							</div>
						</li>
					</ul>
				</section>
			</div>
		</div>
	</div>

	<!-- 
	======================================================= 
	    Full Feature List
	=======================================================
	-->
	<div id="why" class="full-container why-container">
		<div class="skew-prepended"></div>

		<div class="container-fluid">
			<section class="row why">
				<div class="col-sm-12">
					<header>
						<h2><i class="icon-bulb"></i> Why Choose Vetlify</h2>
						<hr class="border-white">
						<p>Our methodology is based on real-life veterinary practice and the result of hundreds of hours of research and development. The features in this app are practical and straight-forward.</p>
					</header>

					<div class="owl-carousel features">

					  	<div class="feature">
					  		<div class="top-img checkup">
					  			<img src="images/physical-checkup.jpg" alt="Physical Checkup">
					  		</div>

					  		<div class="feature-body">
					  			<h3><span>01</span> Interactive Physical Checkup</h3>
					  			<p>Using image mapping, patient records are associated with the exact areas affected; creating a unique user experience.</p>
					  		</div>
					  	</div>

					  	<div class="feature">
					  		<div class="top-img sms">
					  			<img src="images/sms.jpg" alt="SMS Image">
					  		</div>

					  		<div class="feature-body">
					  			<h3><span>02</span> In-App SMS/E-mail Messaging</h3>
					  			<p>Contacting clients regarding appointments, promos and other information can be done with minimal difficulty.</p>
					  		</div>
					  	</div>

					  	<div class="feature">
					  		<div class="top-img appointment">
					  			<img src="images/appointment.jpg" alt="Appointment">
					  		</div>

					  		<div class="feature-body">
					  			<h3><span>03</span> Real-time Appointments</h3>
					  			<p>Clients can make appointments online which is reflected real-time on the clinic app.</p>
					  		</div>
					  	</div>

					  	<div class="feature">
					  		<div class="top-img paperless">
					  			<img src="images/paperless.jpg" alt="Paperless Transaction">
					  		</div>

					  		<div class="feature-body">
					  			<h3><span>04</span> Paperless Transactions</h3>
					  			<p>From adding new patients to updating patient records, all these can be done using the app, minimizing paper usage.</p>
					  		</div>
					  	</div>

					  	<div class="feature">
					  		<div class="top-img pos">
					  			<img src="images/pos.jpg" alt="Point of Sales">
					  		</div>

					  		<div class="feature-body">
					  			<h3><span>05</span> Built-in Point of Sales</h3>
					  			<p>Selling medicine and other supplies is easy using the POS system included in the app.</p>
					  		</div>
					  	</div>

					  	<div class="feature">
					  		<div class="top-img inventory">
					  			<img src="images/stocks.jpg" alt="Stocks">
					  		</div>

					  		<div class="feature-body">
					  			<h3><span>06</span> Clinic Stocks Management</h3>
					  			<p>You can easily check stock levels and add new supplies with this module.</p>
					  		</div>
					  	</div>

					  	<div class="feature">
					  		<div class="top-img reports">
					  			<img src="images/reports.jpg" alt="Reports">
					  		</div>

					  		<div class="feature-body">
					  			<h3><span>07</span> Generate Different Kinds of Reports</h3>
					  			<p>You can easily generate whatever report you may need with our customized reports.</p>
					  		</div>
					  	</div>

					  	<div class="feature">
					  		<div class="top-img clinics">
					  			<img src="images/clinic.jpg" alt="Clinic">
					  		</div>

					  		<div class="feature-body">
					  			<h3><span>08</span> Manage Multiple Clinics</h3>
					  			<p>With this program, you can check on multiple clinics without needing to visit the clinic in person.</p>
					  		</div>
					  	</div>

					</div>
				</div>
			</section>
		</div>

		<div class="skew-appended"></div>
	</div>

	<!-- 
	======================================================= 
	    Reporting Feature
	=======================================================
	-->
	<div id="reporting" class="full-container reporting">
		<div class="container-fluid">
			<div class="row">
				<div class="reporting-contents">
					<header>
						<h2><i class="icon-graph-1"></i> Reporting Made Simple</h2>
						<hr class="border-gray">
						<p>Easily get insight on how your clinic is performing, with our comprehensive reporting tools, you won’t miss any valuable information on tracking your clinic’s performance.</p>
					</header>

					<div class="col-sm-7 image-holder">
						<img src="images/report-img.jpg" alt="Report Image">
					</div>

					<div class="col-sm-5 report-summary">
						<p>Our customizable reports are simple to use and very flexible. You can collate information efficiently.</p>
						
						<p><strong>A few kinds of reports you can generate.</strong></p>
						<ul>
							<li><i class="icon-check"></i> Client Report</li>
							<li><i class="icon-check"></i> Sales Report</li>
							<li><i class="icon-check"></i> Check-up Report</li>
							<li><i class="icon-check"></i> Inventory Report</li>
							<li><i class="icon-check"></i> Appointment Report</li>
							<li><i class="icon-check"></i> Patient Report</li>
							<li><i class="icon-check"></i> Diagnosis</li>
							<li><i class="icon-check"></i> Statistics Report</li>
						</ul>
						<div class="clearfix"></div>
						<p class="many-others">and many others...</p>
					</div>
				</div>	
			</div>
		</div>
	</div>

	<!-- 
	======================================================= 
	    Screenshots
	=======================================================
	-->
	<div id="screenshots" class="full-container screenshots-wrapper">
		<section class="container-fluid">
			<header>
				<h2><i class="icon-frame-picture-streamline"></i> Screenshots</h2>
				<hr class="border-gray">
				<p>Want to look at some of the app's user-interface? Here you go, have a look.</p>
			</header>

			<div class="grid">
				<div class="grid-1 grid-item">
					<a href="images/screenshots/1.jpg">
						<img src="images/screenshots/1-thumb.jpg" alt="Image 1">
					</a>
				</div>

				<div class="grid-2 grid-item">
					<a href="images/screenshots/2.jpg"><img src="images/screenshots/2-thumb.jpg" alt="Image 2"></a>
				</div>

				<div class="grid-3 grid-item">
					<a href="images/screenshots/3.jpg"><img src ="images/screenshots/3-thumb.jpg" alt="Image 3"></a>
				</div>

				<div class="grid-4 grid-item">
					<a href="images/screenshots/4.jpg"><img src ="images/screenshots/4-thumb.jpg" alt="Image 4"></a>
				</div>

				<div class="grid-5 grid-item">
					<a href="images/screenshots/5.jpg"><img src ="images/screenshots/5-thumb.jpg" alt="Image 5"></a>
				</div>
				<div class="grid-6 grid-item">
					<a href="images/screenshots/6.jpg"><img src="images/screenshots/6-thumb.jpg" alt="Image 6"></a>
				</div>

				<div class="grid-7 grid-item">
					<a href="images/screenshots/7.jpg"><img src="images/screenshots/7-thumb.jpg" alt="Image 7"></a>
				</div>

				<div class="grid-8 grid-item">
					<a href="images/screenshots/8.jpg"><img src="images/screenshots/8-thumb.jpg" alt="Image 8"></a>
				</div>
			</div>
		</section>
	</div>

	<!-- 
	======================================================= 
	    Google Map
	=======================================================
	-->
	<div id="contact" class="full-container">
		<section class="inner-container">
			<header>
				<h2><i class="icon-iphone-streamline"></i> Contact Us</h2>
				<hr class="border-gray">
				<p>If you have questions or comments, don’t hesitate to reach us.</p>
			</header>

			<!-- Actual Google Map -->
			<div id="map"></div>
		</section>
	</div>

	<!-- 
	======================================================= 
	    Contact Form & Other details
	=======================================================
	-->
	<div class="full-container contact-wrapper">
		<section class="container contact-form-contents">
			<div class="row">
				<div class="col-sm-6">
					<header>
						<div class="media-left media-middle">
							<img src="images/agent.jpg" alt="Agent" class="img-circle">
						</div>
							
						<div class="media-body">
							<h4>Get in touch</h4>
							<p>We’d like to hear from you, leave a message below and we will respond as soon as possible.</p>
						</div>
					</header>
							
					<div class="contact-form">
						<form action="send-mail" method="post" id="email-form">
							 {{ csrf_field() }}
							<div class="name input-wrapper">
								<input type="text" name="name" class="form-control" placeholder="Your name*">
							</div>
							
							<div class="email input-wrapper">
								<input type="text" name="email" class="form-control" placeholder="Your e-mail address*">
							</div>
							
							<div class="subject input-wrapper">
								<input type="text" name="subject" class="form-control" placeholder="Subject*">
							</div>
							
							<div class="message input-wrapper">
								<textarea name="message" class="form-control" cols="30" rows="10" placeholder="Your message*"></textarea>
							</div>
							
							<div class="send input-wrapper">
								<input type="submit" value="Send message" class="btn btn-primary send-btn">
							</div>
						</form>

						@if (count($errors) > 0)
						    <div class="alert alert-danger">
						        <ul>
						            @foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						            @endforeach
						        </ul>
						    </div>
						@endif
					</div>
				</div>
							
				<div class="col-sm-6 contact-infos">
					<header>
						<h4>Contact Details</h4>
						<p>You can also reach us using the information below.</p>
					</header>
							
					<div class="col-sm-12 location-block info-block">
						<div class="inner-block">
							<div class="media-left">
								<i class="icon-location"></i>
							</div>
							
							<div class="media-right">
								<p>Bldg 1 Unit 1E, Sampaguita St. Tenejero Balanga City, Bataan PH 2100</p>
							</div>
						</div>
					</div>
							
					<div class="col-sm-12 email-block info-block">
						<div class="inner-block">
							<div class="media-left">
								<i class="icon-email-mail-streamline"></i>
							</div>
							
							<div class="media-right">
								<ul>
									<li><a href="">consult@bizwex.com</a></li>
									<li><a href="">vetlify@gmail.com</a></li>
								</ul>
							</div>
						</div>
					</div>
							
					<div class="col-sm-12 phone-block info-block">
						<div class="inner-block">
							<div class="media-left">
								<i class="icon-iphone-streamline"></i>
							</div>
							
							<div class="media-right">
								<ul>
									<li>Landline: (047) 240 4563</li>
									<li>Mobile: +63 917 257 2552</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>

@endsection