<?php

Route::group(['middleware' => ['SecurePage']], function () {
    Route::get('/clients', 'TemplateController@clientList');
    Route::get('/clients/profile/{id}', 'TemplateController@clientProfile');
});
