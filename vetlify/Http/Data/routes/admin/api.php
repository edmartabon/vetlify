<?php

Route::group(['middleware' => ['SecurePage'], 'prefix' => 'api'], function () {
    Route::get('/clients', 'ClientController@index');
    Route::get('/clients/create', 'ClientController@create');
    Route::post('/clients', 'ClientController@store');
    Route::get('/clients/{id}', 'ClientController@show');
    Route::get('/clients/{id}/edit', 'ClientController@edit');
    Route::put('/clients/{id}', 'ClientController@update');
    Route::delete('/clients/{id}', 'ClientController@destroy');
    Route::post('/clients/{id}', 'ClientController@destroy');
});
