<?php

namespace Vetlify\Http\Middlewares;

use Config;
use Closure;
use Sentinel;
use Illuminate\Http\Response;
use Vetlify\Admin\Models\Account;

class SecurePageMiddleware
{
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Sentinel::check()) {
            return $next($request);
        }

        return new Response(view('Vetlify::pages.errors.401'));
    }
}
