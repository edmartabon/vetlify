<?php

namespace Vetlify\Http\Middlewares;

use Config;
use Closure;
use Sentinel;
use Vetlify\Admin\Models\Account;

class LoginMiddleware
{
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Sentinel::check()) {
            return redirect('/'.Config::get('vetlify.app_route_prefix').'/');
        }

        return $next($request);
    }
}
