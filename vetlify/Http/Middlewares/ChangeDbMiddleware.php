<?php

namespace Vetlify\Http\Middlewares;

use Closure;
use VetlifyDatabase;
use Vetlify\Admin\Models\Account;

class ChangeDbMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->changeClientDB($request);
        return $next($request);
    }

    /**
     *
     */
    private function changeClientDB($request)
    {
        VetlifyDatabase::changeDefaultConfigConnection(['database' => 'vetlify']);

        $subdomain = $this->getSubdomainRoute($request);
        $account = Account::leftJoin('databases', 'accounts.database_id', '=', 'databases.id')
            ->where('server', $subdomain)
            ->select('accounts.server as database', 'databases.host', 'databases.port')
            ->first();

        if ($account) {
            return VetlifyDatabase::changeDefaultConfigConnection($account->toArray());
        }

        return abort(404);
    }

    private function getSubdomainRoute($request)
    {
        $route = $request->route();
        return $route->parameter('account');
    }
}
